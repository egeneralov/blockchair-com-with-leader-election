module github.com/egeneralov/blockchair.com-with-leader-election

go 1.12

require (
	github.com/coreos/etcd v3.3.17+incompatible // indirect
	github.com/coreos/pkg v0.0.0-20180928190104-399ea9e2e55f // indirect
	github.com/gogo/protobuf v1.3.1 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/prometheus/client_golang v1.2.1 // indirect
	go.etcd.io/etcd v3.3.17+incompatible
	go.uber.org/zap v1.12.0
)
