package main

import (
	"flag"
	"strings"
  "time"
//   "fmt"
  
  "log"
  "net/http"
  "io/ioutil"
  
	"go.etcd.io/etcd/raft/raftpb"
)


var (
//   BindTo string
//   Payload string
  Period int
  kvs *kvstore
)


func loadPayload() (bool) {
  log.Printf("loadPayload: started")
  req, err := http.NewRequest("GET", "https://api.blockchair.com/stats", nil)
  if err != nil {
    log.Printf("loadPayload: error: %s", err)
    return false
  }
  
  resp, err := http.DefaultClient.Do(req)
  if err != nil {
    log.Printf("loadPayload: error: %s", err)
    return false
  }
  
  if resp.StatusCode != 200 {
    log.Printf("loadPayload: error: status code: %d", resp.StatusCode)
    return false
  }
  
  defer resp.Body.Close()
  body, err := ioutil.ReadAll(resp.Body)
  if err != nil {
    log.Printf("loadPayload: error: %s", err)
    return false
  }
  
  // Payload = string(body)
  
  kvs.Propose("/", string(body))
  
  log.Printf("loadPayload: finished")
  return true
}


func main() {
	cluster := flag.String("cluster", "http://127.0.0.1:9021", "comma separated cluster peers")
	id := flag.Int("id", 1, "node ID")
	kvport := flag.Int("port", 9121, "key-value server port")
	join := flag.Bool("join", false, "join an existing cluster")

//   flag.StringVar(&BindTo, "bind", "0.0.0.0:2819", "golang bind to")
  flag.IntVar(&Period, "period", 60, "refresh info every X seconds")
  
	flag.Parse()

	proposeC := make(chan string)
	defer close(proposeC)
	confChangeC := make(chan raftpb.ConfChange)
	defer close(confChangeC)

	// raft provides a commit stream for the proposals from the http api
	// var kvs *kvstore
	getSnapshot := func() ([]byte, error) { return kvs.GetSnapshot() }
	commitC, errorC, snapshotterReady := newRaftNode(*id, strings.Split(*cluster, ","), *join, getSnapshot, proposeC, confChangeC)
  
  
	kvs = newKVStore(<-snapshotterReady, proposeC, commitC, errorC)
  
  
  
  for {
    if loadPayload() {
      break
    }
    time.Sleep(time.Second)
  }
  
	// the key-value http handler will propose updates to raft
	go serveHttpKVAPI(kvs, *kvport, confChangeC, errorC)
	
  go func() {
    for {
      time.Sleep(time.Second * time.Duration(Period))
      loadPayload()
    }
  }()
	
	for {
  	time.Sleep(time.Second)
/*
  	fmt.Println(&proposeC)
  	fmt.Println(&commitC)
*/
    
    
  
//     sss, _ := kvs.GetSnapshot()
//   	fmt.Println(string(sss))
	}

//   for {}
	
}
